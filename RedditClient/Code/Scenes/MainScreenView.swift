//
//  MainScreenView.swift
//  RedditClient
//
//  Created by Alex Maggio on 14/01/2022.
//

import Foundation
import UIKit

class MainView: UIView {
    private lazy var dataSource = MainDatasource(table: tableView)

    private lazy var tableView: UITableView = {
        let retValue = UITableView()
        retValue.translatesAutoresizingMaskIntoConstraints = false
        retValue.refreshControl = refreshControl
        return retValue
    }()

    private lazy var refreshControl: UIRefreshControl = {
        let retValue = UIRefreshControl()
        return retValue
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupUI()
    }

    private func setupUI() {
        addSubview(tableView)
        NSLayoutConstraint.activate(
            [
                tableView.leadingAnchor.constraint(equalTo: safeAreaLayoutGuide.leadingAnchor),
                tableView.trailingAnchor.constraint(equalTo: safeAreaLayoutGuide.trailingAnchor),
                tableView.topAnchor.constraint(equalTo: safeAreaLayoutGuide.topAnchor),
                tableView.bottomAnchor.constraint(equalTo: safeAreaLayoutGuide.bottomAnchor),
            ]
        )
        tableView.tableFooterView = nil

        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
    }

    @objc func refresh() {
        dataSource.posts = Post.randomList(50)
        refreshControl.endRefreshing()
    }
}
