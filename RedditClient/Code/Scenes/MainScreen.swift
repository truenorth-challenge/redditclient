import Foundation
import UIKit

class MainDatasource: NSObject, UITableViewDataSource {
    var posts: [Post] = Post.randomList() {
        didSet {
            table.reloadData()
        }
    }

    private var table: UITableView

    init(table: UITableView) {
        self.table = table
        super.init()
        self.table.dataSource = self
        self.table.register(PostCell.self, forCellReuseIdentifier: PostCell.identifier)
    }

    func numberOfSections(in tableView: UITableView) -> Int { 1 }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int { posts.count }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let aCell = tableView.dequeueReusableCell(withIdentifier: PostCell.identifier,
                                                        for: indexPath) as? PostCell
        else {
            fatalError("Unable to dequeue PostCell")
        }

        aCell.configure(posts[indexPath.row])
        return aCell
    }
}

class MainViewController: UIViewController {
    private lazy var mainView: MainView = {
        let retValue = MainView()
        retValue.backgroundColor = UIColor.red
        retValue.translatesAutoresizingMaskIntoConstraints = false
        return retValue
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(mainView)
        NSLayoutConstraint.activate(
            [
                mainView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
                mainView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
                mainView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
                mainView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor),
            ]
        )
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        mainView.refresh()
    }
}
