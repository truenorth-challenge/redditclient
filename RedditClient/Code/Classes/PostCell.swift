//
//  PostCell.swift
//  RedditClient
//
//  Created by Alex Maggio on 14/01/2022.
//

import UIKit

class PostCell: UITableViewCell {

    static let identifier: String = "RedditClient.PostCell"

    private lazy var title: UILabel = {
        let retValue = UILabel()
        retValue.font = UIFont.preferredFont(forTextStyle: .title1, compatibleWith: nil)
        retValue.numberOfLines = 0
        return retValue
    }()

    private lazy var author: UILabel = {
        let retValue = UILabel()
        retValue.font = UIFont.preferredFont(forTextStyle: .body, compatibleWith: nil)
        retValue.translatesAutoresizingMaskIntoConstraints = false
        retValue.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        retValue.setContentHuggingPriority(.defaultHigh, for: .vertical)
        return retValue
    }()

    private lazy var postedOn: UILabel = {
        let retValue = UILabel()
        retValue.font = UIFont.preferredFont(forTextStyle: .caption2, compatibleWith: nil)
        retValue.setContentHuggingPriority(.defaultLow, for: .horizontal)
        retValue.setContentHuggingPriority(.defaultLow, for: .vertical)
        return retValue
    }()

    func configure(_ post: Post) {
        title.text = post.title
        author.text = post.author
        postedOn.text = post.timeAgoDisplay()
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        let authorRow = UIStackView(arrangedSubviews: [author, postedOn])
        authorRow.translatesAutoresizingMaskIntoConstraints = false
        authorRow.axis = .horizontal
        authorRow.distribution = .fill
        authorRow.alignment = .lastBaseline
        authorRow.spacing = 4

        let titleRow = UIStackView(arrangedSubviews: [title])

        let dataContainer = UIStackView(arrangedSubviews: [titleRow, authorRow])
        dataContainer.translatesAutoresizingMaskIntoConstraints = false
        dataContainer.axis = .vertical
        dataContainer.spacing = 8

        contentView.addSubview(dataContainer)
        NSLayoutConstraint.activate(
            [
                dataContainer.leadingAnchor.constraint(equalTo: contentView.leadingAnchor),
                dataContainer.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
                dataContainer.topAnchor.constraint(equalTo: contentView.topAnchor),
                dataContainer.bottomAnchor.constraint(equalTo: contentView.bottomAnchor),
            ]
        )
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
