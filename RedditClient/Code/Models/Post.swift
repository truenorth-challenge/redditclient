import Foundation

struct Post: Codable {
    var id: String = ""
    var title: String = ""
    var author: String = ""
    var postedOn: Date = Date()
    var comments: Int = 0
    var isUnread: Bool = false

    var thumbnail_height: Int?
    var thumbnail_width: Int?
    var thumbnailURL: String?

    func timeAgoDisplay() -> String {
        let formatter = RelativeDateTimeFormatter()
        formatter.unitsStyle = .full
        return formatter.localizedString(for: postedOn, relativeTo: Date())
    }


    var thumbnail: RedditImage? {
        RedditImage(height: thumbnail_height ?? 0,
                    width: thumbnail_width ?? 0,
                    rawURL: thumbnailURL
        )
    }

    var fullImage: RedditImage? {
        RedditImage(height: thumbnail_height ?? 0,
                    width: thumbnail_width ?? 0,
                    rawURL: thumbnailURL
        )
    }
}

extension Post {
    static var randomPost: Post {
        var retValue = Post()
        retValue.id = "\(arc4random())"
        retValue.title = "Rabbit  \(arc4random() % 100) with a very very very long text as title spanning several lines"
        retValue.author = "Alex \(arc4random() % 1000)"
        retValue.postedOn = Date(timeIntervalSinceNow: TimeInterval(-60 * Int((arc4random() % 10))))
        retValue.thumbnailURL = "https://a.thumbs.redditmedia.com/uaiJMNY6zMCRgG-gYZhjUla0UCqVadk3lqK7nGM8bi8.jpg"
        retValue.thumbnail_height = 134
        retValue.thumbnail_width = 140
        retValue.comments = Int(arc4random() % 500)
        retValue.isUnread = false
        return retValue
    }

    static func randomList(_ size: Int = 50) -> [Post] {
        var retValue: [Post] = []
        for _ in 0 ..< size { retValue.append(Post.randomPost) }
        return retValue
    }
}
