import Foundation

struct RedditImage {
    let height: Int
    let width: Int
    let rawURL: String?

    var url: URL? {
        guard let rawURL = rawURL else {
            return nil
        }
        return URL(string: rawURL)
    }
}
